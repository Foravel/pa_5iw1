<?php
use Faker\Factory;
use App\Entity\User;
use App\Tests\Func\AbstractTest;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class DiscussionTest extends AbstractTest {

    use RefreshDatabaseTrait;
    
    public function testGetDiscussions() : void
    {
        $response = $this->createClientWithCredentials()->request('GET', '/discussions');
        $this->assertResponseIsSuccessful();
    }

    public function testPostDiscussion() : void
    {
        $faker = Factory::create();
        $response = $this->createClientWithCredentials()->request('POST', '/discussions', [
            'json' => [
                'query' => 'Demande  de remboursement', 
                'isResolved' => false,
                'users' => ['/users/27', 'users/28']
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8'); 
    }

    public function testPatchDiscussion() : void
    {
        $faker = Factory::create();
        $response = $this->createClientWithCredentials()->request('PATCH', '/discussions', [
            'json' => [
                'query' => 'Demande  de remboursement', 
                'isResolved' => false,
                'users' => ['/users/27', 'users/28']
            ]
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8'); 
    }
}