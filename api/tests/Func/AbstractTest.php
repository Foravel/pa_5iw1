<?php


namespace App\Tests\Func;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

abstract class AbstractTest extends ApiTestCase
{

    private $token;
    private $clientWithCredentials;

    public function setUp(): void
    {
        self::bootKernel();
    }

    protected function createClientWithCredentials($token = null, $contentType = 'application/json'): Client
    {
        $token = $token ?: $this->getToken();
        return static::createClient([], ['headers' => [
            'authorization' => 'Bearer ' . $token,
            'content-type' => $contentType
            ]]);
    }

    protected function getToken($body = [])
    {
        if ($this->token) {
            return $this->token;
        }

        $response = static::createClient()->request('POST', '/authentication_token', [
            'headers'=> ['content-type' => 'application/json'],
            'body' => $body ?: json_encode([
            'email' => 'admin@admin.com',
            'password' => '123soleil2'
        ])]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;
        return $this->token;
    }
}
