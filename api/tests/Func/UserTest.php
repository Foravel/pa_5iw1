<?php
use Faker\Factory;
use App\Entity\User;
use App\Tests\Func\AbstractTest;
use App\Tests\Func\AbstractEndpoint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserTest extends AbstractTest {
    
    public function testGetUsers() : void
    {
        $response = $this->createClientWithCredentials()->request('GET', '/users');
        $this->assertResponseIsSuccessful();
    }

    public function testPostUser() : void
    {
        $faker = Factory::create();
        $response = static::createClient()->request('POST', '/users', [
            'json' => [
                'email' => $faker->email(), 
                'password' => $faker->password(), 
                'firstName' => $faker->firstName(),
                'lastName' => $faker->lastName(),
                'phone' => $faker->phoneNumber(),
                'stripePriceId' => 'price_1J6Xb4CHi2hgOXJOeeoks7SU'
            ]
        ]);
  
        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testUpdateUser() : void
    {
        $faker = Factory::create();
        $email = $faker->email();

        $response = static::createClient()->request('POST', '/users', [
            'json' => [
                'email' => $email, 
                'password' => $faker->password(), 
                'firstName' => $faker->firstName(),
                'lastName' => $faker->lastName(),
                'phone' => $faker->phoneNumber(),
                'stripePriceId' => 'price_1J6Xb4CHi2hgOXJOeeoks7SU'
            ]
        ]);

        $client =  $this->createClientWithCredentials(null, 'application/merge-patch+json');
        $iri = $this->findIriBy(User::class, ['email' => $email]);
        $client->request('PATCH', $iri, ['json' => [
            'password' => 'newPassword'
        ]]);
  
        $this->assertResponseIsSuccessful();
    }

    

}