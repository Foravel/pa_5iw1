<?php

namespace App\Tests\Controller;

use App\Entity\Campaign;
use App\Controller\CampaignController;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Faker\Factory;

class CampaignControllerTest extends TestCase
{
    private $campaign;
    private $campaignController;
    private $faker;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->faker = Factory::create();
        $client = $this->createMock(HttpClientInterface::class);
        $this->campaign = $this->createMock(Campaign::class);
        $this->campaignController = new CampaignController($client);

    }

    public function testBuildTitle(): void
    {
        $this->assertInstanceOf(Campaign::class, $this->campaign);
        $this->assertInstanceOf(CampaignController::class, $this->campaignController);

        $this->campaign->method('getTitleParts')->willReturn([
          0 => ['Devis'],
          1 => ['Voiture'],
          2 => ['Paris'],
        ]);
    
        $result = $this->campaignController->buildTitle(1, $this->campaign->getTitleParts(), null, $this->campaign);
        
        $expectedResult = [
          'string-without-tag' => 'Devis Voiture Paris ',
          'parts' => [
            0 => 'Devis',
            1 => 'Voiture',
            2 => 'Paris'
          ],
          'string' => '<h1>Devis Voiture Paris</h1>'
        ];

        $this->assertEquals($expectedResult, $result);
        $expectedResult = '<h1>Devis Voiture Paris</h1>';
        $this->assertEquals($expectedResult, $result['string']);
        
    }

    public function testMarkovify() : void
    {
      $text = $this->faker->text(1000);
  
      //$text = "Un texte répond de façon plus ou moins pertinente à des critères qui en déterminent la qualité littéraire. On retient en particulier la structure d'ensemble, la syntaxe et la ponctuation, l'orthographe lexicale et grammaticale, la pertinence et la richesse du vocabulaire, la présence de figures de style, le registre de langue et la fonction recherchée (narrative, descriptive, expressive, argumentative, injonctive, poétique). C'est l'objet de l'analyse littéraire.";
      $result = $this->campaignController->markovify($text, 1, 2000);
      $this->assertNotNull($result);
      $this->assertIsString($result);
      $this->assertGreaterThan(1, strlen($result));
    }

    function testCleaningContent() : void
    {
      //$text = $this->faker->text(1000);
      $text = "Un texte https://goog le.com r?épond de https://google.com façon p?lus test@gmail.com ou moins pertinente à des critères @ezlfzmfezzm qui en déterminent la qualité littéraire. On retient \$lorel en particulier la structure d'ensemble, la syntaxe et la ponctuation, l'orthographe lexicale et grammaticale, la pertinence et la richesse du vocabulaire, la présence de figures de style, le registre de langue et la fonction recherchée (narrative, descriptive, expressive, argumentative, injonctive, poétique). C'est l'objet de l'analyse littéraire.";
      $this->campaignController->cleaningContent($text);
      $this->assertStringNotContainsString('https://google.com', $text);
      $this->assertStringNotContainsString('https://goog le.com', $text);
      $this->assertStringNotContainsString('test@gmail.com', $text);
      $this->assertStringNotContainsString('@ezlfzmfezzm', $text);
      $this->assertStringNotContainsString('$lorel', $text);
      //var_dump($text);
    }

    function testReplaceNamedEntity() {
      $namedEntity = '[NAMEDENTITY:PERSON]Jean DUJARDIN[/NAMEDENTITY]';
      $text = "Un texte ".$namedEntity." répond de façon plus ou moins pertinente à des critères qui en déterminent la qualité littéraire. On retient en particulier la structure d'ensemble, la syntaxe et la ponctuation, l'orthographe lexicale et grammaticale, la pertinence et la richesse du vocabulaire, la présence de figures de style, le registre de langue et la fonction recherchée (narrative, descriptive, expressive, argumentative, injonctive, poétique). C'est l'objet de l'analyse littéraire.";
      $this->campaignController->replaceNamedEntity($text);
      //var_dump($text);
      $this->assertStringNotContainsString($namedEntity, $text);
    }

    function testHighlightNamedEntity() {
      $namedEntity = '[NAMEDENTITY:PERSON]Jean DUJARDIN[/NAMEDENTITY]';
      $text = "Un texte ".$namedEntity." répond de façon plus ou moins pertinente à des critères qui en déterminent la qualité littéraire. On retient en particulier la structure d'ensemble, la syntaxe et la ponctuation, l'orthographe lexicale et grammaticale, la pertinence et la richesse du vocabulaire, la présence de figures de style, le registre de langue et la fonction recherchée (narrative, descriptive, expressive, argumentative, injonctive, poétique). C'est l'objet de l'analyse littéraire.";
      $this->campaignController->highlightNamedEntity($text);
      //var_dump($text);
      $this->assertStringContainsString('<span style="background-color:yellow">Jean DUJARDIN</span>', $text);
    }

    function testAddDecorationTags() : void
    {
      $backlinks = [
        'https://www.google.com',
        'https://www.bing.com',
      ];
      $tags = ['a','u', 'strong'];
      $text = "Un texte répond de façon plus ou moins pertinente à des critères qui en déterminent la qualité littéraire. On retient en particulier la structure d'ensemble, la syntaxe et la ponctuation, l'orthographe lexicale et grammaticale, la pertinence et la richesse du vocabulaire, la présence de figures de style, le registre de langue et la fonction recherchée (narrative, descriptive, expressive, argumentative, injonctive, poétique). C'est l'objet de l'analyse littéraire.";
      $this->campaign->method('getBacklinks')->willReturn($backlinks);
      $text = $this->campaignController->addDecorationTags($text, $tags, $this->campaign->getBacklinks());
      $containBacklink = false;
      if(str_contains($text,$backlinks[0]) || str_contains($text,$backlinks[1])) {
        $containBacklink = true;
      }
      $this->assertTrue($containBacklink);
      $this->assertStringContainsString($tags[0], $text);
      $this->assertStringContainsString($tags[1], $text);
      $this->assertStringContainsString($tags[2], $text);
    }

    function testBreakIntoParagraph() {
      
      $text = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut. Labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus. saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.";
      $text = $this->campaignController->breakIntoParagraphes($text, 100, 200);

      $this->assertTrue(count(explode('</p>',$text)) == 3);
    }


    /* public function testAddTitleToParagraph(): void
    { 
        $campaignController = new CampaignController();
        $titleParts = [[
            "Devis",
          ],
          [
            "Réparation",
          ],
          [
            "Camion",
          ]
        ];
        $this->campaign = $this->createMock(Campaign::class);
        $this->campaign->method('getSubtitleParts')->willReturn([
            false, true, true
        ]);
        var_dump($campaignController->addTitleToParagraph(2, $titleParts, "Ceci est un paragraphe très court.", ['Devis', 'Réparation', 'Campaign'], $this->campaign));
        

    } */

    
}