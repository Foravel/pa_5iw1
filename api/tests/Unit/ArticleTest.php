<?php

namespace App\Tests\Api;

use App\Entity\User;
use App\Entity\Article;
use App\Entity\Message;
use App\Entity\Discussion;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Campaign;

class ArticleTest extends ApiTestCase
{
    private Article $article;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->article = new Article();
    }

    public function testGetTitle() : void
    {
        $value = 'A example title';
        $response = $this->article->setTitle($value);

        self::assertInstanceOf(Article::class, $response);
        self::assertEquals($value, $this->article->getTitle());
    }
    
    public function testGetContent() : void
    {
        $value = 'An example content';
        $response = $this->article->setContent($value);

        self::assertInstanceOf(Article::class, $response);
        self::assertEquals($value, $this->article->getContent());
    }

    public function testGetImageUrl() : void
    {
        $value = 'https://imageurlexample.com';
        $response = $this->article->setImageurl($value);
        
        self::assertInstanceOf(Article::class, $response);
        self::assertEquals($value, $this->article->getImageurl());
    }

    public function testGetCampaign() : void
    {
        $value = new Campaign();
        $response = $this->article->setCampaign($value);
        
        self::assertInstanceOf(Article::class, $response);
        self::assertEquals($value, $this->article->getCampaign());
    }



}
