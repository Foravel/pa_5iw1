<?php

namespace App\Tests\Api;

use App\Entity\User;
use App\Entity\Article;
use App\Entity\Message;
use App\Entity\Discussion;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Campaign;

class CampaignTest extends ApiTestCase
{
    private Campaign $campaign;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->campaign = new Campaign();
    }

    public function testGetKeywords() : void
    {
        $value = ['A keyword test'];
        $response = $this->campaign->setKeywords($value);

        self::assertInstanceOf(Campaign::class, $response);
        self::assertEquals($value, $this->campaign->getKeywords());
        self::assertCount(1, $this->campaign->getKeywords());    
    }
    
    public function testGetPreview() : void
    {
        $value = 'A test preview';
        $response = $this->campaign->setPreview($value);
        
        self::assertInstanceOf(Campaign::class, $response);
        self::assertEquals($value, $this->campaign->getPreview());
    }
    
    public function testGetTitleParts() : void
    {
        $value = ['A title part'];
        $response = $this->campaign->setTitleParts($value);

        self::assertInstanceOf(Campaign::class, $response);
        self::assertEquals($value, $this->campaign->getTitleParts());
        self::assertCount(1, $this->campaign->getTitleParts());    
    }
    
    public function testGetSubtitleParts() : void
    {
        $value = ['A subtitle part'];
        $response = $this->campaign->setSubtitleParts($value);
        
        self::assertInstanceOf(Campaign::class, $response);
        self::assertEquals($value, $this->campaign->getSubtitleParts());
        self::assertCount(1, $this->campaign->getSubtitleParts());    
    }
    
    public function testGetArticles() {
        $value = new Article();
        $response = $this->campaign->addArticle($value);
        self::assertInstanceOf(Campaign::class, $response);
        self::assertCount(1, $this->campaign->getArticles());
        self::assertTrue($this->campaign->getArticles()->contains($value));
        $response = $this->campaign->removeArticle($value);
        self::assertInstanceOf(Campaign::class, $response);
        self::assertCount(0, $this->campaign->getArticles());
        self::assertFalse($this->campaign->getArticles()->contains($value));
    }
    
    
    
    public function testGetLanguage() : void
    {
        $value = 'FR';
        $response = $this->campaign->setLanguage($value);
        
        self::assertInstanceOf(Campaign::class, $response);
        self::assertEquals($value, $this->campaign->getLanguage());
    }

    public function testGetBacklinks() : void
    {
        $value = ['https://backlinkexample.com'];
        $response = $this->campaign->setBacklinks($value);

        self::assertInstanceOf(Campaign::class, $response);
        self::assertEquals($value, $this->campaign->getBacklinks());
        self::assertCount(1, $this->campaign->getBacklinks());    
    }

}
