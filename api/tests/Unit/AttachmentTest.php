<?php

namespace App\Tests\Api;

use App\Entity\User;
use App\Entity\Message;
use App\Entity\Discussion;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Attachment;
use DateTime;

class AttachmentTest extends ApiTestCase
{
    private Attachment $attachment;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->attachment = new Attachment();
    }

    
    public function testGetUrl() : void
    {
        $value = 'https://example.com';
        $response = $this->attachment->setUrl($value);

        self::assertInstanceOf(Attachment::class, $response);
        self::assertEquals($value, $this->attachment->getUrl());
    }

    public function testGetName() : void
    {
        $value = 'A name';
        $response = $this->attachment->setName($value);

        self::assertInstanceOf(Attachment::class, $response);
        self::assertEquals($value, $this->attachment->getName());
    }

    public function testGetMessage() : void
    {
        $value = new Message();
        $response = $this->attachment->setMessage($value);

        self::assertInstanceOf(Attachment::class, $response);
        self::assertEquals($value, $this->attachment->getMessage());
    } 

}
