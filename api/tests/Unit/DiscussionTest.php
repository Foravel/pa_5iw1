<?php

namespace App\Tests\Api;

use App\Entity\User;
use App\Entity\Message;
use App\Entity\Discussion;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use DateTime;

class DiscussionTest extends ApiTestCase
{
    private Discussion $discussion;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->discussion = new Discussion();
    }

    
    public function testGetMessages() : void
    {
        $value = new Message();
        $response = $this->discussion->addMessage($value);
        self::assertInstanceOf(Discussion::class, $response);
        self::assertCount(1, $this->discussion->getMessages());
        self::assertTrue($this->discussion->getMessages()->contains($value));
        $response = $this->discussion->removeMessage($value);
        self::assertInstanceOf(Discussion::class, $response);
        self::assertCount(0, $this->discussion->getMessages());
        self::assertFalse($this->discussion->getMessages()->contains($value));
    }

    public function testGetCreatedAt() : void
    {
        $value = new DateTime();
        $response = $this->discussion->setCreatedAt($value);

        self::assertInstanceOf(Discussion::class, $response);
        self::assertEquals($value, $this->discussion->getCreatedAt());
    }

    public function testGetUsers() : void
    {
        $value = new User();
        $response = $this->discussion->addUser($value);

        self::assertInstanceOf(Discussion::class, $response);
        self::assertCount(1, $this->discussion->getUsers());
        self::assertTrue($this->discussion->getUsers()->contains($value));
        $response = $this->discussion->removeUser($value);
        self::assertInstanceOf(Discussion::class, $response);
        self::assertCount(0, $this->discussion->getUsers());
        self::assertFalse($this->discussion->getUsers()->contains($value));

    }


    public function testGetQuery() : void
    {
        $value = 'A test query';
        $response = $this->discussion->setQuery($value);

        self::assertInstanceOf(Discussion::class, $response);
        self::assertEquals($value, $this->discussion->getQuery());
    }
    
    public function testGetLastlyOpenedBy() : void
    {
        $value = [new User()];
        $response = $this->discussion->setLastlyOpenedBy($value);
        
        self::assertInstanceOf(Discussion::class, $response);
        self::assertEquals($value, $this->discussion->getLastlyOpenedBy());
        self::assertCount(1, $this->discussion->getLastlyOpenedBy());    
    }
    
    public function testGetIsResolved() : void
    {
        $value = true;
        $response = $this->discussion->setIsResolved($value);

        self::assertInstanceOf(Discussion::class, $response);
        self::assertEquals($value, $this->discussion->getIsResolved());
    }
}
