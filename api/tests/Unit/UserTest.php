<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Discussion;
use App\Entity\Message;
use App\Entity\User;

class UserTest extends ApiTestCase
{
    private User $user;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->user = new User();
    }

    public function testGetEmail() : void
    {
        $value = 'lorel@ipsum.com';
        $response = $this->user->setEmail($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getEmail());
    }
    
    public function testGetPassword() : void
    {
        $value = 'testpassword';
        $response = $this->user->setPassword($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getPassword());
    }

    public function testGetRoles() : void
    {
        $value = ['ROLE_ADMIN'];
        $response = $this->user->setRoles($value);
        
        self::assertInstanceOf(User::class, $response);
        self::assertContains('ROLE_USER', $this->user->getRoles());
        self::assertContains('ROLE_ADMIN', $this->user->getRoles());
    }

    public function testGetFirstName() : void
    {
        $value = 'Léo';
        $response = $this->user->setFirstName($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getFirstName());
    }

    public function testGetLastName() : void
    {
        $value = 'Messi';
        $response = $this->user->setLastName($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getLastName());
    }
    
    public function testGetPhone() : void
    {
        $value = '0625099143';
        $response = $this->user->setPhone($value);
        
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getPhone());
    }
    
    public function testGetMessages() {
        $value = new Message();
        $response = $this->user->addMessage($value);
        self::assertInstanceOf(User::class, $response);
        self::assertCount(1, $this->user->getMessages());
        self::assertTrue($this->user->getMessages()->contains($value));
        $response = $this->user->removeMessage($value);
        self::assertInstanceOf(User::class, $response);
        self::assertCount(0, $this->user->getMessages());
        self::assertFalse($this->user->getMessages()->contains($value));
    }
    
    public function testGetDiscussions() {
        $value = new Discussion();
        $response = $this->user->addDiscussion($value);
        self::assertInstanceOf(User::class, $response);
        self::assertCount(1, $this->user->getDiscussions());
        self::assertTrue($this->user->getDiscussions()->contains($value));
        $response = $this->user->removeDiscussion($value);
        self::assertInstanceOf(User::class, $response);
        self::assertCount(0, $this->user->getDiscussions());
        self::assertFalse($this->user->getDiscussions()->contains($value));
    }
    
    public function testGetQuota() : void
    {
        $value = 100;
        $response = $this->user->setQuota($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getQuota());
    }
}
