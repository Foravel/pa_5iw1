<?php

namespace App\Tests\Api;

use App\Entity\User;
use App\Entity\Message;
use App\Entity\Discussion;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Attachment;
use DateTime;

class MessageTest extends ApiTestCase
{
    private Message $message;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->message = new Message();
    }

    
    public function testGetText() : void
    {
        $value = 'An text example';
        $response = $this->message->setText($value);

        self::assertInstanceOf(Message::class, $response);
        self::assertEquals($value, $this->message->getText());
    }

    public function testGetSender() : void
    {
        $value = new User();
        $response = $this->message->setSender($value);

        self::assertInstanceOf(Message::class, $response);
        self::assertEquals($value, $this->message->getSender());
    }

    public function testGetSentAt() : void
    {
        $value = new DateTime();
        $response = $this->message->setSentAt($value);

        self::assertInstanceOf(Message::class, $response);
        self::assertEquals($value, $this->message->getSentAt());
    }

    public function testGetDiscussion() : void
    {
        $value = new Discussion();
        $response = $this->message->setDiscussion($value);

        self::assertInstanceOf(Message::class, $response);
        self::assertEquals($value, $this->message->getDiscussion());
    }

    public function testGetAttachments() : void
    {
        $value = new Attachment();
        $response = $this->message->addAttachment($value);

        self::assertInstanceOf(Message::class, $response);
        self::assertCount(1, $this->message->getAttachments());
        self::assertTrue($this->message->getAttachments()->contains($value));
        $response = $this->message->removeAttachment($value);
        self::assertInstanceOf(Message::class, $response);
        self::assertCount(0, $this->message->getAttachments());
        self::assertFalse($this->message->getAttachments()->contains($value));
    }



}
