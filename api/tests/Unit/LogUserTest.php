<?php

namespace App\Tests\Api;

use App\Entity\User;
use App\Entity\Message;
use App\Entity\Discussion;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Attachment;
use App\Entity\LogUser;
use DateTime;

class LogUserTest extends ApiTestCase
{
    private LogUser $logUser;

    protected function setUp() : void 
    {
        parent::setUp();
        $this->logUser = new LogUser();
    }

    
    public function testGetIp() : void
    {
        $value = '192.168.0.0';
        $response = $this->logUser->setIp($value);

        self::assertInstanceOf(LogUser::class, $response);
        self::assertEquals($value, $this->logUser->getIp());
    }

    public function testGetLastLoginAt() : void
    {
        $value = new DateTime();
        $response = $this->logUser->setLastLoginAt($value);

        self::assertInstanceOf(LogUser::class, $response);
        self::assertEquals($value, $this->logUser->getLastLoginAt());
    }

}
