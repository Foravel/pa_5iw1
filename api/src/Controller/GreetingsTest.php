<?php

// composer require symfony/http-client

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Greeting;
use App\Service\GreetingService;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GreetingsTest
{
    private $bookPublishingHandler;
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function __invoke(Greeting $data): Response
    {
        $response = $this->client->request(
            'GET',
            'http://api-flask:5000/scrap'
        );

        $statusCode = $response->getStatusCode();
        // $statusCode = 200
        $contentType = $response->getHeaders()['content-type'][0];
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $content = $response->toArray();
        // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]
        return new Response('.');

    }
}