<?php

// composer require symfony/http-client elasticsearch/elasticsearch

namespace App\Controller;

use Faker\Factory;
use App\Entity\Article;
use App\Entity\Campaign;
use App\Service\EncodingService;
use Elasticsearch\ClientBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use function Symfony\Component\VarDumper\Dumper\esc;

class CampaignController extends AbstractController
{
    private $bookPublishingHandler;
    private $client;
    private $campaign;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function __invoke(Campaign $data, Request $request): JsonResponse
    {
        $export = $request->query->get('export') == 1 ? true : false;
        $this->campaign = $data;
        //Scrap texts and save it in ES DB
        if($export != true) {

            $this->client->request(
                'POST',
                $_ENV['API_FLASK_ENDPOINT'].'/scrap',
                [
                    'json' => [
                        'campaign_id' => $data->getId(),
                        'campaign_keywords' => $data->getKeywords(),
                        'campaign_language' => $data->getLanguage()
                        ]
                    ]
                );
                    
        }
        // Retrieve text in ES DB and generate new text from it with markov algorithm

        $hosts = [
            $_ENV['ELASTICSEARCH_DATABASE_ENDPOINT']
        ];

        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

        $params = [
            'index' => 'text',
            'body' => [
                'size' => 1000,
                'query' => [
                    'match' => [
                        'campaign_id' => $data->getId()
                        //'campaign_id' => '88'
                    ]
                ]
            ]
        ];

        $response = $client->search($params);
        $textJoined = ''; 
        
        foreach ($response['hits']['hits'] as $hit) {
            $textJoined .= (trim($hit['_source']['text'])) . ' ';
        }
        
        $this->cleaningContent($textJoined);
        if($export) {
            for($i=0;$i<50;$i++) {
                $content = $this->generateContentFromText($textJoined, $data);
                $this->replaceNamedEntity($content['content']);
                $this->exportContent($content, $this->campaign);
            }
            //$faker = Factory::create();
            $generationToken = hash('sha256', $this->campaign->getId());
            return new JsonResponse($generationToken);
        }

        $content = $this->generateContentFromText($textJoined, $data);
        $this->highlightNamedEntity($content['content']);

        $entityManager = $this->getDoctrine()->getManager();
        $campaign = $data;
        $campaign->setPreview($content['content']);
        $entityManager->persist($campaign);
        $entityManager->flush();

        return new JsonResponse('$textFinal');
    }

    public function cleaningContent(&$content) {
        // Remove indesirable question mark
        preg_match_all('/[a-z,\.\s]+(\?)[a-z]+/', $content, $matches);
        foreach($matches[1] as $match) {
            $content = str_replace($match, str_replace('?', '', $match), $content);
        }

        // Remove website url with possible blank space
        $content = preg_replace('/((https?)\:\/\/)?(www\.)?[a-z0-9\.]*\s*[a-z0-9\.]*\.[a-z]+/', '', $content);

        // Remove email with possible blank space
        $content = preg_replace('/([^\s])*@.*\.[a-zA-Z]+/', '', $content);

        // Remove all $[a-zA-Z]
        $content = preg_replace('/\$[a-zA-Z]+/', '', $content);

        // Remove all @XXXXXX
        $content = preg_replace('/@[A-Za-z0-9]/', '', $content);
    }

    public function replaceNamedEntity(&$content) {
        // Replace all underscore by blank spaces
        preg_match_all('/\[NAMEDENTITY\:([A-Z_\s]*)\]([^\[\]\/]*)\[\/NAMEDENTITY\]/', $content, $matches);
        foreach($matches[2] as $match) {
            $content = str_replace($match, str_replace('_', ' ', $match), $content);
        }

        // Extract the named entity type
        $n = preg_match_all('/\[NAMEDENTITY\:([A-Z_\s]*)\]([^\[\]\/]*)\[\/NAMEDENTITY\]/', $content, $matches);
        foreach($matches[1] as $key => $match) {
            $faker = Factory::create();
            // If type is PERSON replace the named entity with a fake proper noun
            if($match == 'PERSON') {
                $content  = str_replace($matches[0][$key], $faker->name(), $content);
            // If type is ORGANISATION replace the named entity with a fake company
            } elseif ($match == 'ORG') {
                $content  = str_replace($matches[0][$key], $faker->company(), $content);
            } else {
                $content  = str_replace($matches[0][$key], '', $content);
            }
        }

    }

    public function highlightNamedEntity(&$content) {
        // Replace all underscore by blank spaces
        preg_match_all('/\[NAMEDENTITY\:[A-Z_\s]+\]([^\[\]\/]*)\[\/NAMEDENTITY\]/', $content, $matches);
        foreach($matches[1] as $match) {
            $content = str_replace($match, str_replace('_', ' ', $match), $content);
        }

        // Replace [NAMEDENTITY] tag by <span> tag
        preg_match_all('/\[NAMEDENTITY\:[A-Z_\s]+\]/', $content, $matches);
        foreach($matches[0] as $match) {
            $content = str_replace($match, '<span style="background-color:yellow">', $content);
        }

        $content = str_replace('[/NAMEDENTITY]', '</span>', $content);


    }

    public function exportContent($content, $campaign) {
        $article = new Article();
        $article->setTitle($content['title']);
        $article->setContent($content['content']);
        $r = rand(0,count($this->campaign->image)-1);
        $imgUrl = $_ENV['BASE_URL'].'/media/'.$this->campaign->image[$r]->filePath;
        $article->setImageurl($imgUrl);
        $article->setCampaignIdHashed(hash('sha256', $campaign->getId()));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();
        return $article->getCampaignIdHashed();
    }

    public function generateContentFromText($textJoined, $data) {
        $text = $textJoined;
        $text = $this->markovify(($text), 1, 2000);
        $h1 = $this->buildTitle(1, $data->getTitleParts(), null, $this->campaign);
        $r = rand(0,count($this->campaign->image)-1);
        $img = $_ENV['BASE_URL'].'/media/'.$this->campaign->image[$r]->filePath;
        $title = $h1['string'];
        $textFinal = $this->breakIntoParagraphes($text, 100, 200);
        $textFinal = $this->addDecorationTags($textFinal, ['a','u', 'strong'], $this->campaign->getBacklinks());
        $textFinal = EncodingService::fixUTF8($textFinal);
        $textFinal = $this->addTitles($textFinal, $h1['parts']);
        $textFinal = '<img style="width:100%" src="'.$img.'"/>'.$textFinal;
        $textFinal = $h1['string'].$textFinal;

        return ['content' => $textFinal, 'title' => $h1['string-without-tag'], 'img' => $img];
    }

    // Automated text generator using markov chain
    public function markovify($text, $keySize, $maxWords)
    {
        // Create list of words
        $words = array();
        $position = 0;
        $maxPosition = strlen($text);

        // Store every word of the text in an array
        while ($position < $maxPosition) {
            // Regex to get string begining with a white space (= word)
            if (preg_match('/^(\S+)/', substr($text, $position, strlen($text)), $matches)) {
                // The string that matched the regex is the first word of the subject and is stored in token array
                $words[] = $matches[1];
                // Calculate an offset to prevent the previous word to match again with the regex
                $position += strlen($matches[1]);
            } elseif (preg_match('/^(\s+)/', substr($text, $position, strlen($text)), $matches)) {
                $position += strlen($matches[1]);
            } else {
                die('Unknown token found at position ' . $position . ' : ' .
                    substr($text, $position, strlen($text)) . '...' . PHP_EOL);
            }
        }
        
        // CREATE DICTIONNARY
        $dictionary = array();

        // Iterates through every words
        for ($i = 0; $i < count($words) - $keySize; $i++) {
            $prefix = '';
            $separator = '';

            // Using a loop seems to be useless as I use a keysize of 1
            for ($c = 0; $c < $keySize; $c++) {
                $prefix .= $separator . $words[$i + $c];
                $separator = '.';
            }

            // Make the current word as an array key and associate to this key a value that is an array containing the next word
            $suffix = $words[$i + $keySize];
            $dictionary[$prefix][] = $suffix;
            
            // When a word is repeated multiple time he will therefore have as many suffixes as the word is repeated
            
        }
        
        /*
            Exemple de dictionnaire: La voiture est belle, cette voiture date des années 1990
            $dictionary = [
                'La' => ['Voiture'],
                'Voiture' => ['est', 'date'], 
                ...
                ]
            Voiture is repeated twice in the text, so it can have 2 suffixes
        */

        // CHOOSING A RANDOM STARTING WORD
        $rand = rand(0, count($words) - $keySize);
        $startWord = array();

        // Using a loop seems to be useless as I use a keysize of 1
        for ($c = 0; $c < $keySize; $c++) {
            array_push($startWord, $words[$rand + $c]);
        }

        // CREATE TEXT
        $text = implode(' ', $startWord); // The starting word (exemple : 'Voiture')
        $keySize = $keySize;
        do {
            $wordsKey = implode('.', $startWord);

            // Si le prefix existe, on lui choisit un suffixe aléatoire
            if (array_key_exists($wordsKey, $dictionary)) {

                $rand = rand(0, count($dictionary[$wordsKey]) - 1);
                $newWord = $dictionary[$wordsKey][$rand];
                $text .= ' ' . $newWord;
                $keySize++;
                // Le suffixe choisi devient alors la nouvelle valeur de $startWord
                array_shift($startWord);
                array_push($startWord, $newWord);
            } else {
                $keySize++;
            }
        } while ($keySize < $maxWords);

        return $text;
    }

 //public function testtest() { return 'hello';}

    public function buildTitle($level = 1, $titleParts, $paragraph = null, $campaign, $h1Part = null)
    {
        if ($level == 1) {
            $title = [];
            $title['string-without-tag'] = '';
            foreach ($titleParts as $key => $part) {
                $title['parts'][] = $part[array_rand($part)];
                $title['string-without-tag'] .= $part[array_rand($part)] . ' ';
            }
            $title['string'] = '<h' . $level . '>' . trim($title['string-without-tag']) . '</h' . $level . '>';
            return $title;
        } elseif ($level == 2) {
            $title = '';
            // Create an array containing only the keys of True element in subtitleParts[]
            $titlePartsKeys = array_keys(array_filter($campaign->getSubtitleParts(), function ($el) use ($campaign) {
                return ($el);
            }));
            foreach ($titlePartsKeys as $key) {
                $title .= $h1Part[$key] . ' '; // Prefix the h2 with parts of h1 
            }
            $title = trim($title);
            $title .= ' | ';
            foreach ($titlePartsKeys as $key) {
                $title .= $titleParts[$key][array_rand($titleParts[$key])] . ' ';
            }
            $title = trim($title);
            return '<h' . $level . '>' . $title . '</h' . $level . '>';;
        } else {
            $title = '';
            $firstSentenceEnd = strpos($paragraph, '.');
            return '<h' . $level . '>' . substr($paragraph, 0, $firstSentenceEnd) . '</h' . $level . '>';;
        }
    }

    public function addTitleToParagraph($level, $titleParts, $paragraph, $h1Part = null, $campaign)
    {
        return $this->buildTitle($level, $titleParts, $paragraph, $campaign, $h1Part) . '<p>' . $paragraph . '</p>';
    }

    #[Route(
        name: 'kjhj',
        path: '/pkwg',
        methods: ['GET']
    )]
    public function test() : JsonResponse { 


        $results = [
            'aaaa' => [
                'regergergr',
                'egergegeg'
            ],
            'bbbb' => [
                'regergergr',
                'egergegeg'
            ]
        ];
        
        

        return new JsonResponse($results);
    }

    public function addDecorationTags($text, $tags, $backlinks = []) {
        $tagPositions = $this->getOpeningClosingTagPositions($text);
        if(is_array($tagPositions) == false) return; 
        $offset = 0;
        
        foreach($tagPositions as $key => $tagPos) {
            $tag = $tags[rand(0,count($tags)-1)];
            if($tag == 'a') {
                $url = $backlinks[array_rand($backlinks)];
                $openingTag = '<a href="'.$url.'">';
                $closingTag = '</a>';
                } else{
                $openingTag = '<'.$tag.'>';
                $closingTag = '</'.$tag.'>';
            }

            $text = substr_replace($text, $openingTag, $tagPos[0]+$offset, 0); 
            $text = substr_replace($text, $closingTag, $tagPos[1]+$offset+strlen($openingTag), 0); 
            $offset += strlen($openingTag) + strlen($closingTag);
        }
        return $text;
      }

    public function getOpeningClosingTagPositions($text) {
        $needle = " ";
        $lastPos = 0;
        $positions = array();

        // Get all blanks spaces positions in an array
        while (($lastPos = strpos($text, $needle, $lastPos))!== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }
        // Handle Text too short
        if(count($positions) <= 1) return false;


        // Divide array in equal arrays, each array contains a number of elements equal to 50% of initial array length
        //$nbElementsPerArray = round(count($positions) * 0.1);
        $nbElementsPerArray = round(count($positions) * 0.1);
        $chunkedPositions = array_chunk($positions, $nbElementsPerArray);
        $allPositions = [];

        // In each little arrays, select 2 randoms elements : 1st should be in first half of the array
        foreach($chunkedPositions as $key => $cp) {
            $randIndexFirstHalf = rand(0, round(count($cp)/2)) - 1;
            $randIndexSecondHalf = $randIndexFirstHalf+5;
            if(array_key_exists($randIndexFirstHalf,  $cp) == false) continue; 
            while($randIndexSecondHalf >  count($cp) - 1) $randIndexSecondHalf -= 1;
            if(array_key_exists($randIndexSecondHalf,  $cp) == false) continue; 
            $openingTagPosition = $cp[$randIndexFirstHalf];
            $closingTagPosition = $cp[$randIndexSecondHalf];
            // Ensure that closingtagpos is strictly higher than openingtag position
            if($closingTagPosition == $openingTagPosition || $closingTagPosition < $openingTagPosition) continue;
            array_push($allPositions, [$openingTagPosition, $closingTagPosition]);
        }
        
        return $allPositions;
      
      }
      
   
      
    public function breakIntoParagraphes($text, $minWords, $maxWords) {
        $sentences = explode('.', $text);
        $skipUntilKey = 0;
        $sentencesCount = count($sentences);
      
        foreach($sentences as $key => $sentence) {
      
          if($key < $skipUntilKey) continue;
          // Sentence word count is between max and min make this sentence as paragraph
          if(count(explode(' ', $sentence)) > $minWords && count(explode(' ', $sentence)) < $maxWords) {
            $sentences[$key] = '<p>'.ucfirst($sentence).'.</p>';
            continue;
          }
          
          // Sentence word count is higher than max, make this sentence a paragraph
          if(count(explode(' ', $sentence)) > $maxWords) {
            $sentences[$key] = '<p>'.ucfirst($sentence).'.</p>';
            continue;
          }
      
          $i = 1;
      
          while (count(explode(' ', $sentence)) < $minWords && $key+$i < $sentencesCount ) {
            $sentence = $sentence . ' .' . $sentences[$key+$i];
            unset($sentences[$key+$i]);
            $sentences[$key] = '<p>'.ucfirst($sentence).'.</p>';
            $skipUntilKey = $key+$i+1;
            $i++;
          }
      
        }
      
        //echo '<pre>'; var_dump($sentences); echo '</pre>';
        return implode('', $sentences);
      }
      
    public function addTitles($text, $h1Parts) {
        // Get all paragraph beginning positions
        $needle = '<p>';
        $lastPos = 0;
        $positions = [];
        while (($lastPos = strpos($text, $needle, $lastPos))!== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
        }

        $offset = 0;
        foreach($positions as $key => $pos) {
          if($key % 3 == 0) {
            $title = $this->buildTitle(2, $this->campaign->getTitleParts(), null, $this->campaign, $h1Parts);
            $text = substr_replace($text, $title, $pos+$offset, 0); 
            $offset += strlen($title);
          }
        }
      
        return $text;
      }
      
      
}
