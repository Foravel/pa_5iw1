<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\DiscussionRepository;
use App\Repository\MessageRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class MailController  extends AbstractController
{

    private $mailer;
    
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(User $data = null, Request $request, DiscussionRepository $repo) : Response
    {

        
        $emailToSend = $request->query->get('emailToSend');
        $emailRecever= ($data == null) ? $request->query->get('to') : $data->getEmail();

        $mailTemplates = [
            'forgot_password' => 'emails/change-password.html.twig',
            'checkout_confirmation' => 'emails/checkout-confirmation.html.twig',
            'discussion_transcript' => 'emails/discussion-transcript.html.twig'
        ];

        $mailTitles = [
            'forgot_password' => 'Change your password',
            'checkout_confirmation' => 'Checkout confirmation',
            'discussion_transcript' => 'Transcript de la conversation'
        ];

        $discussionId = $request->query->get('d') ?? null;

        $specificDatas = [
            'forgot_password' => null,
            'checkout_confirmation' => null,
            'discussion_transcript' => $this->getDiscussion($discussionId, $repo)
        ];

        $message = (new \Swift_Message($mailTitles[$emailToSend]))
            ->setFrom('raveloson.fanilo@gmail.com')
            ->setTo($emailRecever)
            ->setBody(
                $this->renderView(
                    $mailTemplates[$emailToSend],
                    ['user' => $data, 'email' => $emailRecever, 'datas' => $specificDatas[$emailToSend]]
                ),
                'text/html'
            );

            /* $message = (new \Swift_Message('efzzefz'))
            ->setFrom('raveloson.fanilo@gmail.com')
            ->setTo('raveloson@gmail.com')
            ->setBody(
                $this->renderView(
                    'emails/email-sn.html'
                ),
                'text/html'
            );  */
            
        return new Response(json_encode($this->mailer->send($message)));
    }

    public function getDiscussion($id = null, DiscussionRepository $repo = null) {
        if($id == null) return;
        return $repo->findOneBy(['id' => $id]);
    }
}