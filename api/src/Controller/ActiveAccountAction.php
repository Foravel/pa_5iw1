<?php

namespace App\Controller;

use App\Entity\Greeting;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ActiveAccountAction
{

    public function __construct()
    {
        
    }

    public function __invoke(UserRepository $repo, Request $request): JsonResponse
    {

        $user = $repo->findOneBy(['email' => $request->query->get('email')]);
        $user->setFirstName('true');
      
        return new JsonResponse($user->getEmail());
    }
}