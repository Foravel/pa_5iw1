<?php

namespace App\Entity;

use App\Controller\MeController;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\MailController;
use App\Repository\UserRepository;
use App\Controller\ActiveAccountAction;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
#[ApiResource(
    mercure: true, 
    itemOperations: [
        'patch',
        'get',
        'post_publication' => [
            'method' => 'GET',
            'path' => '/users/{id}/sendMail',
            'controller' => MailController::class,
        ]
    ],
    collectionOperations: [
        'get',
        'post',
        'me' => [
            'method' => 'GET',
            'path' => '/me',
            'controller' => MeController::class,
            'read' => false
        ],
        'send_mail_password_forgotten' => [
            'method' => 'GET',
            'path' => '/sendMail',
            'controller' => MailController::class,
            'read' => false
        ],
        'active_account' => [
            'method' => 'GET',
            'path' => '/active-account',
            'controller' => ActiveAccountAction::class,
            'read' => false
        ]
    ],
    normalizationContext: ['groups' => ['user']]
)]
#[ApiFilter(SearchFilter::class, properties: ['email' => 'exact'])]
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user", "message", "discussion"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user", "discussion"})
     */
    private $email;

    /**
     * @var array
     * 
     * @ORM\Column(type="json")
     * @Groups({"user"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"discussion", "message", "user"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @groups({"discussion", "user"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @groups({"user"})
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity=SubscriptionPlan::class, inversedBy="users")
     * @groups({"user"})
     */
    private $subscriptionPlan; // Useless

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @groups({"user"}) 
     */
    private $clientSecret; //Useless

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @groups({"user"}) 
     */
    private $didFirstPasswordChange;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $expectedEmail; // Useless

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="sender")
     * @groups({"user"}) 
     */
    private $messages;

    /**
     * @ORM\ManyToMany(targetEntity=Discussion::class, mappedBy="users")
     * @Groups({"user"})
     */
    private $discussions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @groups({"user"})
     */
    private $source;


    /**
     * @ORM\OneToOne(targetEntity=LogUser::class, mappedBy="theUser", cascade={"persist"})
     */
    private $logUser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stripePriceId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @groups({"user"})
     */
    private $stripeSubscriptionId;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 100})
     * @groups({"user"})
     */
    private $quota = 100;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @groups({"user"})
     */
    private $isActivated;
    
    /**
     * @ORM\OneToMany(targetEntity=Campaign::class, mappedBy="theUser")
     * @groups({"user"})
     */
    private $campaigns;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->discussions = new ArrayCollection();
        $this->campaigns = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getSubscriptionPlan(): ?SubscriptionPlan
    {
        return $this->subscriptionPlan;
    }

    public function setSubscriptionPlan(?SubscriptionPlan $subscriptionPlan): self
    {
        $this->subscriptionPlan = $subscriptionPlan;

        return $this;
    }

    public function getClientSecret(): ?string
    {
        return $this->clientSecret;
    }

    public function setClientSecret(?string $clientSecret): self
    {
        $this->clientSecret = $clientSecret;

        return $this;
    }

    public function getDidFirstPasswordChange(): ?bool
    {
        return $this->didFirstPasswordChange;
    }

    public function setDidFirstPasswordChange(?bool $didFirstPasswordChange): self
    {
        $this->didFirstPasswordChange = $didFirstPasswordChange;

        return $this;
    }

    public function getExpectedEmail(): ?string
    {
        return $this->expectedEmail;
    }

    public function setExpectedEmail(?string $expectedEmail): self
    {
        $this->expectedEmail = $expectedEmail;

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setSender($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getSender() === $this) {
                $message->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Discussion[]
     */
    public function getDiscussions(): Collection
    {
        return $this->discussions;
    }

    public function addDiscussion(Discussion $discussion): self
    {
        if (!$this->discussions->contains($discussion)) {
            $this->discussions[] = $discussion;
            $discussion->addUser($this);
        }

        return $this;
    }

    public function removeDiscussion(Discussion $discussion): self
    {
        if ($this->discussions->removeElement($discussion)) {
            $discussion->removeUser($this);
        }

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getLogUser(): ?LogUser
    {
        return $this->logUser;
    }

    public function setLogUser(?LogUser $logUser): self
    {
        // unset the owning side of the relation if necessary
        if ($logUser === null && $this->logUser !== null) {
            $this->logUser->setTheUser(null);
        }

        // set the owning side of the relation if necessary
        if ($logUser !== null && $logUser->getTheUser() !== $this) {
            $logUser->setTheUser($this);
        }

        $this->logUser = $logUser;

        return $this;
    }

    public function getStripePriceId(): ?string
    {
        return $this->stripePriceId;
    }

    public function setStripePriceId(?string $stripePriceId): self
    {
        $this->stripePriceId = $stripePriceId;

        return $this;
    }

    public function getStripeSubscriptionId(): ?string
    {
        return $this->stripeSubscriptionId;
    }

    public function setStripeSubscriptionId(?string $stripeSubscriptionId): self
    {
        $this->stripeSubscriptionId = $stripeSubscriptionId;

        return $this;
    }

    public function getQuota(): ?int
    {
        return $this->quota;
    }

    public function setQuota(?int $quota): self
    {
        $this->quota = $quota;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->isActivated;
    }

    public function setIsActivated(?bool $isActivated): self
    {
        $this->isActivated = $isActivated;

        return $this;
    }

    /**
     * @return Collection|Campaign[]
     */
    public function getCampaigns(): Collection
    {
        return $this->campaigns;
    }

    public function addCampaign(Campaign $campaign): self
    {
        if (!$this->campaigns->contains($campaign)) {
            $this->campaigns[] = $campaign;
            $campaign->setTheUser($this);
        }

        return $this;
    }

    public function removeCampaign(Campaign $campaign): self
    {
        if ($this->campaigns->removeElement($campaign)) {
            // set the owning side to null (unless already changed)
            if ($campaign->getTheUser() === $this) {
                $campaign->setTheUser(null);
            }
        }

        return $this;
    }
}
