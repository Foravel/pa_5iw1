<?php

namespace App\Entity;

use App\Entity\MediaObject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\CampaignController;
use App\Repository\CampaignRepository;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CampaignRepository::class)
 */
#[ApiResource(mercure: true, itemOperations: [
    'patch',
    'get',
    'post_publication' => [
        'method' => 'GET',
        'path' => '/campaign/{id}/scrap',
        'controller' => CampaignController::class,
    ],
])]

class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $keywords = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $prefixes = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $suffixes = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $preview;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $titleParts = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $subtitleParts = [];

    /**
     * @var MediaObject|null
     *
     * @ORM\ManyToMany(targetEntity=MediaObject::class)
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     */
    public $image;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="campaign")
     */
    private $articles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $language;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $backlinks = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name = '';

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="campaigns")
     */
    private $theUser;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->createdAt = new DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeywords(): ?array
    {
        return $this->keywords;
    }

    public function setKeywords(array $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getPrefixes(): ?array
    {
        return $this->prefixes;
    }

    public function setPrefixes(?array $prefixes): self
    {
        $this->prefixes = $prefixes;

        return $this;
    }

    public function getSuffixes(): ?array
    {
        return $this->suffixes;
    }

    public function setSuffixes(?array $suffixes): self
    {
        $this->suffixes = $suffixes;

        return $this;
    }

    public function getPreview(): ?string
    {
        return $this->preview;
    }

    public function setPreview(?string $preview): self
    {
        $this->preview = $preview;
        return $this;
    }

    public function getTitleParts(): ?array
    {
        return $this->titleParts;
    }

    public function setTitleParts(?array $titleParts): self
    {
        $this->titleParts = $titleParts;

        return $this;
    }

    public function getSubtitleParts(): ?array
    {
        return $this->subtitleParts;
    }

    public function setSubtitleParts(?array $subtitleParts): self
    {
        $this->subtitleParts = $subtitleParts;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setCampaign($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getCampaign() === $this) {
                $article->setCampaign(null);
            }
        }

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getBacklinks(): ?array
    {
        return $this->backlinks;
    }

    public function setBacklinks(?array $backlinks): self
    {
        $this->backlinks = $backlinks;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTheUser(): ?User
    {
        return $this->theUser;
    }

    public function setTheUser(?User $theUser): self
    {
        $this->theUser = $theUser;

        return $this;
    }

}
