-- Adminer 4.8.1 PostgreSQL 13.3 dump

DROP TABLE IF EXISTS "user";
CREATE TABLE "public"."user" (
    "id" integer NOT NULL,
    "subscription_plan_id" integer,
    "email" character varying(180) NOT NULL,
    "roles" json NOT NULL,
    "password" character varying(255) NOT NULL,
    "first_name" character varying(255) NOT NULL,
    "last_name" character varying(255) NOT NULL,
    "phone" character varying(255),
    "client_secret" character varying(255),
    "did_first_password_change" boolean,
    "expected_email" character varying(255),
    "source" character varying(255),
    "stripe_price_id" character varying(255),
    "stripe_subscription_id" character varying(255),
    CONSTRAINT "uniq_8d93d649e7927c74" UNIQUE ("email"),
    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_8d93d6499b8ce200" ON "public"."user" USING btree ("subscription_plan_id");

INSERT INTO "user" ("id", "subscription_plan_id", "email", "roles", "password", "first_name", "last_name", "phone", "client_secret", "did_first_password_change", "expected_email", "source", "stripe_price_id", "stripe_subscription_id") VALUES
(15,	NULL,	'popol@popol.Fr',	'[]',	'123soleil',	'TEST',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(16,	NULL,	'kjklmkjhjk@ljhbn.fr',	'[]',	'123soleil',	'bariakluze',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(17,	NULL,	'lmkjhgbvb@lmkj.fr',	'[]',	'123soleil',	'poihgf',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(18,	NULL,	'kljknkljk@kljn.fr',	'[]',	'123soleil',	'uhjjkjhbn,',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(19,	NULL,	'cacacaca@acacaca.fr',	'[]',	'test',	'caca',	'Undefined',	NULL,	NULL,	'1',	NULL,	'chatbox',	NULL,	NULL),
(0,	NULL,	'admin@admin.com',	'["ROLE_ADMIN"]',	'123soleil2',	'Julien',	'Caffort',	'0625091934',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(20,	NULL,	'lkjhlmlkjhg@lmkjh.fr',	'[]',	'123soleil',	'likujhg',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(21,	NULL,	'lkkklk@lk.fr',	'[]',	'123soleil',	'lk,lk,',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(22,	NULL,	'oklm@oklm.fr',	'[]',	'123soleil',	'oklm',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(23,	1,	'fanilo@digiplace.fr',	'[]',	'123soleil2',	'fanilo',	'raveloson',	'0625091934',	'',	'1',	NULL,	NULL,	NULL,	NULL),
(24,	1,	'lkjlmj@kljhbk.fr',	'[]',	'123soleil',	'ijkhjjkjkh',	'kjkjkjkj',	'0625091934',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(25,	1,	'lmkjhg@mlkj.fr',	'[]',	'123soleil',	'mlkjhg',	'lkjhgklk',	'0625091935',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(26,	1,	'kjhgghjkl@lkjhb.fr',	'[]',	'123soleil',	'lkjhg',	'mlkjhg',	'0625091934',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(27,	1,	'lkjhjjkmlj@hkljhgh.fr',	'[]',	'123soleil',	'zlkjhgh',	'lkjhjkljhj',	'0625091935',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(28,	1,	'lkjj@lkj.fr',	'[]',	'123soleil',	'lkjkljkj',	'kjkljkj',	'0625091935',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(29,	1,	'kljhg@lkjh.fr',	'[]',	'123soleil',	'lkjhg',	'lmkjhklmkjnb',	'0625091935',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(30,	1,	'mkljhgbv@pmolkjh.fr',	'[]',	'123soleil',	'lkjhg',	'lmkjhklmkjh',	'0808080808',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(31,	1,	'lkjkhlkj@lkjh.fr',	'[]',	'123soleil',	'ljhjkjkh',	'kljhjkljh',	'0678787878',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(36,	1,	'admizn@admin.com',	'[]',	'123soleil',	'zfzefzf',	'fzfzf',	'0909090909',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(38,	1,	'lkejflekjlkfj@ljekferf.Fr',	'[]',	'123soleil',	'kljjekljejl',	'jlkjflekfjeklfejlkfjkl',	'0625091934',	'',	'0',	NULL,	NULL,	NULL,	NULL),
(39,	NULL,	'lkjhghkkjhg@kjhg.fr',	'[]',	'123soleil',	'lkjhglkjhg',	'lkjhgjklmkjhg',	'090909099',	'',	'0',	NULL,	NULL,	'price_1IrgYiCHi2hgOXJOmRUaOslm',	NULL),
(40,	NULL,	'lkjhklmj@hkkljhghk.fr',	'[]',	'123soleil',	'mlkjhklmkjh',	'OOOOOH',	'09090909',	'',	'0',	NULL,	NULL,	'price_1IrgYiCHi2hgOXJOmRUaOslm',	NULL),
(48,	NULL,	'test@mep.fr',	'[]',	'123soleil',	'test',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(41,	NULL,	'fanilo.moche@chim.fr',	'[]',	'123soleil2',	'fanilo',	'raveloson',	'062897979',	'pi_1J6gABCHi2hgOXJOUKwz4HAL_secret_7SJeyA7E5fBSy49NbZtzjVGW7',	'1',	NULL,	NULL,	'price_1J6XdHCHi2hgOXJONjLPaLFQ',	'sub_JkAVHV1qirxoNG'),
(49,	NULL,	'popo@popol.fr',	'[]',	'123soleil',	'CACA',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(42,	NULL,	'fanilo.laid@gmail.com',	'[]',	'123soleil2',	'Fanilo',	'Est Moche',	'0625091934',	'pi_1J6usqCHi2hgOXJOLDZE1hvX_secret_EzJgvvOUnlah0RByA2G5qFZW4',	'1',	NULL,	NULL,	'price_1J6Xb4CHi2hgOXJOeeoks7SU',	'sub_JkPi9OjzHIxGZ3'),
(43,	NULL,	'popol@popol.com',	'[]',	'123soleil',	'kefkkfk',	'mkferkfmekmrfk',	'0625091934',	'pi_1J6x1DCHi2hgOXJOLMjOk6aW_secret_l9N5c3YJ6Glo4OUD2PGQvF1tR',	'0',	NULL,	NULL,	'price_1J6Xb4CHi2hgOXJOeeoks7SU',	'sub_JkRvhTj79LNxct'),
(44,	NULL,	'adad@dad.fr',	'[]',	'123soleil',	'adadad',	'adadad',	'062509918',	'pi_1J6xDjCHi2hgOXJOK51eisuA_secret_ZdBmXwDH9Ka9wcCARAMxub4rU',	'0',	NULL,	NULL,	'price_1J6Xb4CHi2hgOXJOeeoks7SU',	'sub_JkS7GgqQpUe8zb'),
(45,	NULL,	'kljhgjk@lkjhgcv.fr',	'[]',	'123soleil',	'poiuytfg',	'ljkhgjkomijhg',	'0625091935',	'pi_1J6xH1CHi2hgOXJOVbyQ6dnt_secret_PEe1LwhHf9rJnirZ3XHRpfJdB',	'0',	NULL,	NULL,	'price_1J6XdHCHi2hgOXJONjLPaLFQ',	'sub_JkSBPUwMQEv1Xz'),
(46,	NULL,	'fghjk@fghjk.fr',	'[]',	'123soleil',	':mlkjh',	'fghjkl',	'0909090909',	'pi_1J6xHsCHi2hgOXJOiqvU5Z5k_secret_MX7QCenKJYAmAFKwLZ8P4C7CR',	'0',	NULL,	NULL,	'price_1J6Xb4CHi2hgOXJOeeoks7SU',	'sub_JkSC9N7ZtBWy5D'),
(47,	NULL,	'omlikujhgpoilkhj@poik.fr',	'[]',	'123soleil',	'moilukhjgopiuk',	'hjgomljkhglmoiljkhj',	'062509913',	'pi_1J6xJlCHi2hgOXJO1a5DpMNR_secret_9AtFnD6VMIoErcoAecAl7ycbQ',	'0',	NULL,	NULL,	'price_1J6Xb4CHi2hgOXJOeeoks7SU',	'sub_JkSEtogv6XHTH5'),
(50,	NULL,	'lkjkomlkj@olkjl.fr',	'[]',	'123soleil',	'khjgolikjh',	'Undefined',	NULL,	NULL,	NULL,	NULL,	'chatbox',	NULL,	NULL),
(3,	NULL,	'admin2@admin.com',	'[]',	'123soleil',	'admin',	'admin',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	NULL,	'admin4@admin.com',	'["ROLE_ADMIN"]',	'123soleil',	'admin',	'admin',	NULL,	NULL,	NULL,	NULL,	NULL,	'test',	NULL);

ALTER TABLE ONLY "public"."user" ADD CONSTRAINT "fk_8d93d6499b8ce200" FOREIGN KEY (subscription_plan_id) REFERENCES subscription_plan(id) NOT DEFERRABLE;

-- 2021-07-03 17:47:40.991146+00